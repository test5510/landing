<?php

namespace App\EventListener;

use App\DTO\PageVisitData;
use App\Service\ActivityService;
use Carbon\Carbon;

use Symfony\Component\HttpKernel\Event\ControllerEvent;

class PageLoadListener
{
    private ActivityService $activityService;

    public function __construct(
        ActivityService $activityService
    ) {
        $this->activityService = $activityService;
    }

    public function onKernelController(ControllerEvent $event)
    {
        $request = $event->getRequest();
        $uri = $request->getRequestUri();
        $pageVisitData = new PageVisitData($uri, new Carbon());

        $this->activityService->postActivity($pageVisitData);
    }
}
