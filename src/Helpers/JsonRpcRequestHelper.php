<?php

namespace App\Helpers;

use Carbon\Carbon;
use Datto\JsonRpc\Http\Client;
use Datto\JsonRpc\Http\Exceptions\HttpException;
use Datto\JsonRpc\Http\HttpResponse;
use Datto\JsonRpc\Responses\ErrorResponse;
use ErrorException;
use Symfony\Component\Config\Definition\Exception\Exception;

class JsonRpcRequestHelper
{
    /**
     * Disable ssl for local test purposes
     *
     * @var array $arrContextOptions
     */
    private array $arrContextOptions = [
        "ssl" => [
            "verify_peer"      => false,
            "verify_peer_name" => false,
        ],
    ];

    /**
     * @param string $method
     * @param array  $params
     *
     * @return bool|HttpResponse|string
     * @throws ErrorException
     * @throws HttpException
     */
    public function sendJsonRpcRequest(string $method, array $params)
    {
        $response = Carbon::now()->timestamp;
        $client = new Client($_ENV['ACTIVITY_URL'], null, $this->arrContextOptions);
        $client->query($method, $params, $response);

        try {
            $client->send();
        } catch (HttpException | ErrorException $exception) {
            throw $exception;
        }

        if ($response instanceof ErrorResponse) {
            throw new Exception('Code: '.$response->getCode().' '.$response->getMessage());
        }

        return $response;
    }
}
