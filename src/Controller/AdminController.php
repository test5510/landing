<?php

namespace App\Controller;

use App\DTO\ActivityFilter;
use App\Service\ActivityService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    private ActivityService $activityService;

    public function __construct(ActivityService $activityService)
    {
        $this->activityService = $activityService;
    }

    /**
     * Получение списка посещений
     *
     * @Route("/activity", methods={"GET"}, name="admin_activity")
     * @param Request $request
     *
     * @return Response
     */
    public function getActivityList(
        Request $request
    ): Response {
        $filterData = ActivityFilter::fromArray($request->query->all());
        $data = $this->activityService->getActivityList($filterData);

        $columns = ['quantity', 'url', 'dateTime'];

        return $this->render('activity.html.twig', ['data' => $data, 'columns' => $columns]);
    }
}
