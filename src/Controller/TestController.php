<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/test")
 */
class TestController extends AbstractController
{
    /**
     * @Route("/first", methods={"GET"}, name="test_first")
     *
     * @return Response
     * @throws \Exception
     */
    public function getFirst(): Response {
        $number = random_int(0, 100);

        return new Response(
            '<html lang="en"><body>Lucky number: '.$number.'</body></html>'
        );
    }

    /**
     * @Route("/second", methods={"GET"}, name="test_second")
     *
     * @return Response
     * @throws \Exception
     */
    public function getSecond(): Response {
        $number = random_int(0, 100);

        return new Response(
            '<html lang="en"><body>Lucky number: '.$number.'</body></html>'
        );
    }
}
