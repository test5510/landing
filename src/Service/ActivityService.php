<?php

namespace App\Service;

use App\DTO\ActivityFilter;
use App\DTO\PageVisitData;
use App\Helpers\JsonRpcRequestHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ActivityService
{
    const VISIT_LIST_METHOD   = 'visitList';
    const VISIT_LOGGER_METHOD = 'visitLogger';

    private EntityManagerInterface $entityManager;
    private HttpClientInterface    $client;
    private JsonRpcRequestHelper   $jsonRpcRequestHelper;

    public function __construct(
        EntityManagerInterface $entityManager,
        HttpClientInterface $client,
        JsonRpcRequestHelper $jsonRpcRequestHelper
    ) {
        $this->entityManager = $entityManager;
        $this->client = $client;
        $this->jsonRpcRequestHelper = $jsonRpcRequestHelper;
    }

    public function getActivityList(ActivityFilter $filterData)
    {
        return $this->jsonRpcRequestHelper->sendJsonRpcRequest(self::VISIT_LIST_METHOD, $filterData->toArray());
    }

    public function postActivity(PageVisitData $pageVisitData)
    {
        return $this->jsonRpcRequestHelper->sendJsonRpcRequest(self::VISIT_LOGGER_METHOD, $pageVisitData->toArray());
    }
}
