<?php

declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

class ValidationException extends \Exception
{
    protected ConstraintViolationListInterface $violations;

    public function __construct(ConstraintViolationListInterface $violations, string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->violations = $violations;
    }

    public static function fromViolationList(
        ConstraintViolationListInterface $violations,
        string $msg = 'Validation error'
    ): self {
        return new self($violations, $msg);
    }

    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
